<?php
$dirname = dirname( __DIR__ ) ;

require_once( $dirname . '/vendor/autoload.php' ) ;

Application::prepare( $dirname . '/config.json' ) ;
Application::execute( ) ;