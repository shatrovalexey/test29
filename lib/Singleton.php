<?php
	/**
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	* @package Test29
	*/
	namespace Test29 ;

	/**
	* Абстрактный класс для реализации паттерна singleton.
	* @var $__instance object - единственный экземпляр
	* @var $__prepared object - контроль одноразовой подготовки объекта к выполнению
	*/
	abstract class Singleton {
		private static $__instance = null ;
		private static $__prepared = false ;

		/**
		* Закрытые конструктор и клонирование объекта.
		*/
		private function __construct( ) { }
		private function __clone( ) { }

		/**
		* Метод инициализации единственного экземпляра
		*/
		private static function __init( ) {
			if ( empty( static::$__instance ) ) {
				static::$__instance = new self( ) ;
			}

			return static::$__instance ;
		}

		/**
		* контроль одноразовой подготовки объекта к выполнению
		*/
		public static function prepare( $data ) {
			static::__init( ) ;

			return static::$__instance->__prepared ;
		}
	}