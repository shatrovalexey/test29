<?php
	/**
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	* @package Test29
	*/
	namespace Test29 ;

	/**
	* Фасад задачи.
	* @var $__config - настройки фасада
	*/
	class Application extends Singleton {
		public $__config = array( ) ;

		/**
		* Загрузка настроек из файла
		* @param $config string - файл настроек в JSON
		* @return object - настройки в виде иерархии обектов stdClass и свойств
		* @throws Exception
		*/
		private static function __configure( $config ) {
			$result = null ;

			if ( $contents = file_get_contents( $config ) ) {
				$result = json_decode( $contents ) ;
			}
			if ( empty( $result ) ) {
				throw new \Exception( 'Counldn\t configure from "' . $config . '"' ) ;
			}

			return $result ;
		}

		/**
		* Загрузка настроек и создание объектов
		* @param $config string - файл настроек в JSON
		* @return bool - при первом вызове возвращает true, при последующих - false.
		*/
		public static function prepare( $config ) {
			if ( parent::prepare( $config ) ) {
				return false ;
			}

			$self = self::$__instance ;
			$config = $self->__config = self::__configure( $config ) ;

			if ( ! empty( $config->include ) ) {
				foreach ( $config->include as $include_name => $include_path ) {
					$config->include->$include_name = self::__configure( $include_path ) ;
				}
			}

			/**
			* @param $ctrl object - контроллер из настроек
			*/
			$ctrl = $data->route->main->ctrl = new $data->route->main->ctrl( ) ;
			$ctrl->prepare( $config ) ;
			Server::prepare( $config ) ;

			$self->__prepared = true ;

			return true ;
		}

		/**
		* Запуск фасада
		* @return void
		*/
		public static function execute( ) {
			Server::execute( ) ;
		}
	}