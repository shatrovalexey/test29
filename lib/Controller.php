<?php
	/**
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	* @package Test29
	*/
	namespace Test29 ;

	/**
	* Контроллер
	*/
	class Controller {
		/**
		* Загрузка настроек и создание объектов
		* @param $config object - объект настроек из файла JSON
		* @return void
		*/
		public function prepare( $config ) {
			DBA::prepare( $config ) ;
		}

		/**
		* Запрос к API инициализации /init - скрипт должен удалять все таблицы из БД, если такие имеются и создавать все необходимые таблицы, после чего внести в них тестовые данные: не менее 5 водителей, не менее 10 автомобилей, не менее 30 записей в истории заказов, не менее 3х заказов “в процессе”.
		*/
		public function getInit( ) {
			return DBA::reinit( ) ;
		}

		/**
		* Найти всех водителей, которые не имеют ни одного заказа за всё время работы (минимум 2 в ответе)
		*/
		public function getDriverOrphant( ) {
			return DBA::entity( 'v_driver_orphant' ) ;
		}

		/**
		* Найти все адреса, которые не имеют успешного статуса и имена операторов, которые их приняли (минимум 5 записей в ответе, операторы могут повторяться)
		*/
		public function getAddressOrphant( ) {
			return DBA::entity( 'v_address_orphant' ) ;
		}

		/**
		* Найти всех водителей, которые имеют более $count выполненных заказов (“Not Found”)
		* @param $count int минимальное количество выполненных заказов -1 заказ
		*/
		public function getDriverOrdersByCount( $count = null ) {
			$sql = '
SELECT
	`vdo1`.*
FROM
	`v_driver_orders_count` AS `vdo1`
			' ;
			$args = array( ) ;

			if ( ! is_null( $count ) ) {
				$args[] = $count ;
				$sql .= '
WHERE
	( `vdo1`.`order_count` > ? )
				' ;
			}

			return DBA::select( $sql , $args ) ;
		}

		/**
		* Найти список автомобилей, на которых работают более 1го водителя и менее 4х водителей (минимум 2 варианта)
		* @param $count_from int минимальное количество водителей +1 водитель
		* @param $count_from int максимальное количество водителей -1 водитель
		*/
		public function getCarByDriverCount( $count_from , $count_to ) {
			return DBA::select( '
SELECT
	`vcdc1`.`id_car`
FROM
	`v_car_driver_count` AS `vcdc1`
WHERE
	( `vcdc1`.`driver_count` BETWEEN ? AND ? ) ;
			' , array(
				$count_from ,
				$count_to
			) ) ;
		}
		/**
		* Список всех сотрудников службы такси (водителей и операторов)
		*/
		public function getEmployee( ) {
			$result = array(
				'driver' => DBA::entity( 'driver' ) ,
				'operator' => DBA::entity( 'operator' )
			) ;
		}
	}