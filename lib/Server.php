<?php
	/**
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	* @package Test29
	*/
	namespace Test29 ;

	/**
	* Обёртка RESTFUL-сервера.
	* @var $__srv \RestService\Server - объект для обработки маршрутизации и запросов
	* @var $__running bool - контроль одноразового запуска сервера
	*/
	class Server extends Singleton {
		private $__srv = null ;
		private $__running = false ;

		/**
		* Загрузка настроек и создание объектов
		* @param $config object - объект настроек из файла JSON
		* @return void
		*/
		public static function prepare( $config ) {
			if ( parent::prepare( $config ) ) {
				return self::$__instance ;
			}

			self::$__instance->__srv = \RestService\Server::create( $config->route->main->path , $config->route->main->ctrl )->
				setDebugMode( $config->debug_mode ) ;

			foreach ( $config->route->method as $http_method => $routes ) {
				$method_name = sprintf( $config->route->method_name , ucfirst( strToLower( $http_method ) ) ) ;

				foreach ( $routes as $path => $ctrl_method ) {
					self::$__instance->__srv->$method_name( $path , $ctrl_method ) ;
				}
			}

			self::$__instance->__srv->done( ) ;
		}

		/**
		* Запуск сервера
		* @return void
		*/
		public static function execute( ) {
			if ( ! empty( self::$__instance->__running ) ) {
				return ;
			}

			self::prepare( ) ;
			self::$__instance->__srv->run( ) ;
			self::$__instance->__running = true ;
		}
	}