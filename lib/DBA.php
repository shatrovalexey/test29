<?php
	/**
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	* @package Test29
	*/
	namespace Test29 ;

	/**
	* Работа с СУБД
	* @var $__dbh mysqli_connection - подключение к СУБД
	* @var $__config object - настройки в виде объекта JSON
	*/
	class DBA extends Singleton {
		private $__dbh ;
		private $__config ;

		/**
		* Проверка существания и создание подключения
		* @return mysqli_connection
		*/
		private static function __connection( ) {
			$self = self::$__instance ;

			if ( ! empty( $self->__dbh ) && $self->__dbh->ping( ) ) {
				return $self->__dbh ;
			}

			$self->__dbh = new \mysqli(
				$self->__config->db->host ,
				$self->__config->db->user ,
				$self->__config->db->passwd ,
				$self->__config->db->name ,
				$self->__config->db->port
			) ;

			return $self->__dbh ;
		}

		/**
		* Предварительная настройка
		* $config object - настройки в виде объекта JSON
		*/
		public static function prepare( $config ) {
			if ( parent::prepare( $config ) ) {
				return self::$__instance ;
			}

			self::$__instance->__config = $config ;
		}

		/**
		* Повторное создание структуры БД из файла дампа БД
		*/
		public static function reinit( ) {
			$self = self::$__instance ;
			$importer = new \MySQLImporter(
				$self->__config->db->host ,
				$self->__config->db->user ,
				$self->__config->db->passwd ,
				$self->__config->db->port
			) ;
			$importer->doImport(
				$self->__config->db->file ,
				$self->__config->db->name
			) ;
		}

		/**
		* Преобразование данной строки в строку, подходящую для использования в SQL-запросах
		* @param $str string - строка
		* @return string
		*/
		private function __escape( $str ) {
			if ( is_null( $str ) ) {
				return ' null ' ;
			}

			return "'" . $this->__dbh->real_escape_string( $str ) . "'" ;
		}

		/**
		* Подстановка аргументов в SQL вместо '?'
		* @param $sql string - SQL-запрос
		* @param $args array - список аргументов
		* @return string
		*/
		private static function __parse( $sql , $args = array( ) ) {
			if ( empty( $args ) ) {
				return $sql ;
			}

			$query = array( ) ;
			foreach ( explode( '?' , $sql ) as $part ) {
				$query[] = $part ;

				if ( empty( $args ) ) {
					continue ;
				}

				$query[] = self::$__instance->__escape( array_shift( $args ) ) ;
			}

			return implode( '' , $query ) ;
		}

		/**
		* Выполнение SQL-запроса в СУБД
		* @param $sql string - SQL-запрос
		* @param $args array - список аргументов
		* @return mysqli_result
		*/
		private static function __query( $sql , $args = array( ) ) {
			$self = self::$__instance ;
			$dbh = self::__connection( ) ;
			$query = self::__parse( $sql , $args ) ;

			return $dbh->query( $query ) ;
		}

		/**
		* Выполнение SQL-запроса на выборку в СУБД
		* @param $sql string - SQL-запрос
		* @param $args array - список аргументов
		* @return array - записи в виде ассоциативных массивов
		*/
		public static function select( $sql , $args = array( ) ) {
			$result = array( ) ;
			foreach ( self::__query( $sql , $args ) as $row ) {
				$result[] = $row ;
			}

			return $result ;
		}

		/**
		* Выполнение SQL-запроса на выборку в БД по имени сущности в БД
		* @param $name string
		* @return array - записи в виде ассоциативных массивов
		*/
		public static function entity( $name ) {
			return self::select( '
SELECT
	`t1`.*
FROM
	`' . $name . '` AS `t1` ;
			' ) ;
		}
	}